# Browserstack testing private applications without publicly exposing your app

Testing your application with BrowserStack while running your application
in your private network. Keeping your code private.

## Usage

### Prerequisites
- You have Docker and docker-compose installed

### Set up
- Copy file `.env.dist` to `.env`
- Update the contents of `.env` with your own credentials
- Execute `docker-compose up -d localconnection`
- Execute `docker run -v "$PWD":/app -w /app node:10 npm i --silent`

### Run your tests
- Execute `docker-compose up cypress`
  - This command will start a container with default nginx webserver and
    a cypress/base container with browserstack-cypress-cli dependency installed.

## BrowserStack Local
BrowserStack has a feature called `Local Testing` which creates a network
connection between the BrowserStack cloud and your testing environment.

- https://www.browserstack.com/docs/automate/cypress/local-testing/ci-cd
- https://hub.docker.com/r/browserstack/local

This project will use this feature to create a connection to only(!) the
server instance to test. It makes use of the Gitlab runner using docker
executor, which, by default, creates a private network per job.
Adding a `Local Testing`-connection to each job ensures that the 
BrowserStack cloud will never access other parts of the network.

## Gitlab Pipeline
By adding an alias to the container running your application, you can use
url https://web in your Cypress tests to access the application.

### gitlab-ci.yml
```
browserstack:
  image: your-image-containing-browserstackLocal-binary-and-browserstack-npm-package
  services:
    - name: nginx
      alias: web
  stage: test
  script:
    - echo "{\"projectId\":\"${CI_PROJECT_NAME}\"}" > cypress.json
    # Start local connection and give extra time to connect
    - /browserstack-local/BrowserStackLocal --key ${BROWSERSTACK_ACCESS_KEY} --local-identifier ${BROWSERSTACK_LOCAL_IDENTIFIER} &
    - sleep 2
    # Start tests
    - browserstack-cypress run --sync
# Uncomment the line below if you like manual control over when to run your tests
#  when: manual
```

## Development
- Run npm install: `docker run -v "$PWD":/app -w /app node:10 npm i`
  - Add `--silent` at the end if you like less output
- Start local connection: `docker-compose up -d localconnection`
- Start tests: `docker-compose up cypress`

### Local connection
After you have set up this project, the Local-connection dashboard is available
 at http://localhost:45454.

### Log
Execute `docker-compose logs -f` to show current log entries for all running 
 containers and keep following (-f) all other messages from that point onward. 
Hit ctrl-c to stop and return to the command line. 

## Useful links
- https://www.browserstack.com/docs/automate/cypress/supported-versions
- https://www.browserstack.com/docs/automate/cypress/browsers-and-os
