FROM cypress/base:10

ENV CI=1

COPY ./package.json /app/package.json
COPY ./package-lock.json /app/package-lock.json

WORKDIR /app

# Install the BrowserStack Cypress CLI
RUN npm install -g browserstack-cypress-cli \
    && npm ci \
    # check Cypress binary path and cached versions
    # useful to make sure we are not carrying around old versions
    && npx cypress cache path \
    && npx cypress cache list \
    && npm run cy:verify \
    && npm run cy:info \
    && mkdir /browserstack-local \
    && curl https://www.browserstack.com/browserstack-local/BrowserStackLocal-linux-x64.zip --output bs.zip --silent \
    && unzip bs.zip -d /browserstack-local

CMD ["npm", "run", "e2e"]
